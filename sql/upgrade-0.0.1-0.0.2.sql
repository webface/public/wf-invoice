ALTER TABLE `invoices` ADD COLUMN `p_name` varchar(255)  NOT NULL AFTER `c_egn`,
 ADD COLUMN `p_city` varchar(255)  NOT NULL AFTER `p_name`,
 ADD COLUMN `p_address` varchar(255)  NOT NULL AFTER `p_city`,
 ADD COLUMN `p_eik` varchar(255)  NOT NULL AFTER `p_address`,
 ADD COLUMN `p_ddsnumber` varchar(255)  AFTER `p_eik`,
 ADD COLUMN `p_mol` varchar(255)  NOT NULL AFTER `p_ddsnumber`;

ALTER TABLE `invoices` ADD COLUMN `p_bank` varchar(255)  AFTER `p_mol`,
 ADD COLUMN `p_iban` varchar(255)  AFTER `p_bank`,
 ADD COLUMN `p_bic` varchar(255)  AFTER `p_iban`;

ALTER TABLE `invoices` ADD COLUMN `p_zdds` TINYINT(1)  DEFAULT NULL;

ALTER TABLE `proforms` ADD COLUMN `p_name` varchar(255)  NOT NULL AFTER `c_egn`,
 ADD COLUMN `p_city` varchar(255)  NOT NULL AFTER `p_name`,
 ADD COLUMN `p_address` varchar(255)  NOT NULL AFTER `p_city`,
 ADD COLUMN `p_eik` varchar(255)  NOT NULL AFTER `p_address`,
 ADD COLUMN `p_ddsnumber` varchar(255)  AFTER `p_eik`,
 ADD COLUMN `p_mol` varchar(255)  NOT NULL AFTER `p_ddsnumber`;

ALTER TABLE `proforms` ADD COLUMN `p_bank` varchar(255)  AFTER `p_mol`,
 ADD COLUMN `p_iban` varchar(255)  AFTER `p_bank`,
 ADD COLUMN `p_bic` varchar(255)  AFTER `p_iban`;

ALTER TABLE `proforms` ADD COLUMN `p_zdds` TINYINT(1)  DEFAULT NULL;
