/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

ALTER TABLE `invoices` ADD COLUMN `prefix` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `id`,
 ADD COLUMN `number` INTEGER UNSIGNED DEFAULT 0 AFTER `prefix`;

UPDATE `invoices` SET `number`=`id`;

delimiter |

CREATE TRIGGER invoice_numbering BEFORE INSERT ON invoices
  FOR EACH ROW BEGIN
       DECLARE next_id INT;
       DECLARE check_id INT;
       SET check_id = (SELECT MAX(number) FROM invoices WHERE prefix = NEW.prefix) + 1;
       IF check_id > 0 THEN SET next_id = check_id;
       ELSE SET next_id = 1;
       END IF;
       SET NEW.number = next_id;
  END;
|

delimiter ;

INSERT INTO `roles` SET `role`="invoices_settings", `role_name`="Настройки на фактурите", id="3";
INSERT INTO `roles` SET `role`="global_settings", `role_name`="Общи настройки", id="4";
INSERT INTO `user_roles` SET `user_id`=1, `role_id`=3;
INSERT INTO `user_roles` SET `user_id`=1, `role_id`=4;

ALTER TABLE `invoices` ADD COLUMN `author` varchar(255)  NOT NULL AFTER `p_zdds`,
 ADD COLUMN `author_user` INTEGER UNSIGNED NOT NULL AFTER `author`;

UPDATE `invoices` SET `author`=(SELECT `realname` FROM `users` WHERE `id`=1);
UPDATE `invoices` SET `author_user`=1;

ALTER TABLE `invoices` ADD COLUMN `author_sign` char(5)  NOT NULL AFTER `author_user`;
UPDATE `invoices` SET `author_sign`=(SELECT `sign_prefix` FROM `users` WHERE `id`=1);


ALTER TABLE `proforms` ADD COLUMN `author` varchar(255)  NOT NULL AFTER `p_zdds`,
 ADD COLUMN `author_user` INTEGER UNSIGNED NOT NULL AFTER `author`;

UPDATE `proforms` SET `author`=(SELECT `realname` FROM `users` WHERE `id`=1);
UPDATE `proforms` SET `author_user`=1;

ALTER TABLE `proforms` ADD COLUMN `author_sign` char(5)  NOT NULL AFTER `author_user`;
UPDATE `proforms` SET `author_sign`=(SELECT `sign_prefix` FROM `users` WHERE `id`=1);

ALTER TABLE `invoices_items` MODIFY COLUMN `mesurement` VARCHAR(255)  CHARACTER SET utf8 COLLATE utf8_general_ci;
UPDATE `invoices_items` SET `mesurement` = CASE `mesurement` WHEN 0 THEN "бр." WHEN 1 THEN "кг." WHEN 2 THEN "л." ELSE "Други" END;

ALTER TABLE `proforms_items` MODIFY COLUMN `mesurement` VARCHAR(255)  CHARACTER SET utf8 COLLATE utf8_general_ci;
UPDATE `proforms_items` SET `mesurement` = CASE `mesurement` WHEN 0 THEN "бр." WHEN 1 THEN "кг." WHEN 2 THEN "л." ELSE "Други" END;

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_methods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_methods`
--

LOCK TABLES `payment_methods` WRITE;
/*!40000 ALTER TABLE `payment_methods` DISABLE KEYS */;
INSERT INTO `payment_methods` VALUES (0,'В брой'),(1,'По банков път'),(6,'С насрещно прихващане'),(5,'Чек / Ваучер'),(4,'Платежно нареждане'),(3,'С карта'),(2,'Наложен платеж'),(7,'Други методи');
/*!40000 ALTER TABLE `payment_methods` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
