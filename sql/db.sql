-- MySQL dump 10.13  Distrib 5.1.61, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: freeinfoice
-- ------------------------------------------------------
-- Server version	5.1.61-0ubuntu0.11.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contractors`
--

DROP TABLE IF EXISTS `contractors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `eik` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dds` tinyint(1) DEFAULT NULL,
  `ddsnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mol` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person` tinyint(1) DEFAULT '0',
  `egn` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currency` char(3) NOT NULL,
  `rate` decimal(10,7) unsigned NOT NULL,
  `sign` char(5) NOT NULL,
  `longsign` varchar(255) NOT NULL,
  `subsign` char(5) NOT NULL,
  `default_c` tinyint(1) NOT NULL DEFAULT '0',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `g` char(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` VALUES (2,'EUR','1.9558300','€','евро','ц.',0,'2012-02-08 14:09:42','но,е'),(1,'BGN','1.0000000','лв.','лв.','ст.',1,'2012-03-12 11:45:19','ин,а');
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prefix` int(10) unsigned NOT NULL DEFAULT '0',
  `number` int(10) unsigned DEFAULT '0',
  `contractor` int(11) NOT NULL,
  `issue_date` date NOT NULL,
  `event_date` date NOT NULL,
  `receiver` varchar(255) NOT NULL,
  `bank_payment` int(10) unsigned NOT NULL DEFAULT '0',
  `vat` decimal(4,2) unsigned NOT NULL,
  `novatreason` varchar(255) DEFAULT NULL,
  `currency` int(11) NOT NULL,
  `rate` decimal(10,7) unsigned NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `related_invoice` varchar(255) DEFAULT NULL,
  `related_date` date DEFAULT NULL,
  `c_name` varchar(255) NOT NULL,
  `c_city` varchar(255) NOT NULL,
  `c_address` varchar(255) NOT NULL,
  `c_eik` varchar(255) DEFAULT NULL,
  `c_ddsnumber` varchar(255) DEFAULT NULL,
  `c_mol` varchar(255) DEFAULT NULL,
  `c_person` tinyint(1) DEFAULT NULL,
  `c_egn` varchar(255) DEFAULT NULL,
  `p_name` varchar(255) NOT NULL,
  `p_city` varchar(255) NOT NULL,
  `p_address` varchar(255) NOT NULL,
  `p_eik` varchar(255) NOT NULL,
  `p_ddsnumber` varchar(255) DEFAULT NULL,
  `p_mol` varchar(255) NOT NULL,
  `p_bank` varchar(255) DEFAULT NULL,
  `p_iban` varchar(255) DEFAULT NULL,
  `p_bic` varchar(255) DEFAULT NULL,
  `p_zdds` tinyint(1) DEFAULT NULL,
  `author` varchar(255) NOT NULL,
  `author_user` int(10) unsigned NOT NULL,
  `author_sign` char(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER |
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER invoice_numbering BEFORE INSERT ON invoices
  FOR EACH ROW BEGIN
       DECLARE next_id INT;
       DECLARE check_id INT;
       SET check_id = (SELECT number FROM invoices WHERE prefix = NEW.prefix order by id DESC limit 1) + 1;
       IF check_id > 0 THEN SET next_id = check_id;
       ELSE SET next_id = 0;
       END IF;
       SET NEW.number = next_id;
  END */|
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `invoices_items`
--

DROP TABLE IF EXISTS `invoices_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `quantity` decimal(12,4) unsigned NOT NULL,
  `mesurement` varchar(255) DEFAULT NULL,
  `price` decimal(13,4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `payment_methods`
--

DROP TABLE IF EXISTS `payment_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_methods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_methods`
--

LOCK TABLES `payment_methods` WRITE;
/*!40000 ALTER TABLE `payment_methods` DISABLE KEYS */;
INSERT INTO `payment_methods` VALUES (0,'В брой'),(1,'По банков път'),(6,'С насрещно прихващане'),(5,'Чек / Ваучер'),(4,'Платежно нареждане'),(3,'С карта'),(2,'Наложен платеж'),(7,'Други методи');
/*!40000 ALTER TABLE `payment_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proforms`
--

DROP TABLE IF EXISTS `proforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proforms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contractor` int(11) NOT NULL,
  `issue_date` date NOT NULL,
  `bank_payment` int(11) NOT NULL DEFAULT '0',
  `vat` decimal(4,2) unsigned NOT NULL,
  `novatreason` varchar(255) DEFAULT NULL,
  `currency` int(11) NOT NULL,
  `rate` decimal(10,7) unsigned NOT NULL,
  `c_name` varchar(255) NOT NULL,
  `c_city` varchar(255) NOT NULL,
  `c_address` varchar(255) NOT NULL,
  `c_eik` varchar(255) DEFAULT NULL,
  `c_ddsnumber` varchar(255) DEFAULT NULL,
  `c_mol` varchar(255) DEFAULT NULL,
  `c_person` tinyint(1) DEFAULT NULL,
  `c_egn` varchar(255) DEFAULT NULL,
  `p_name` varchar(255) NOT NULL,
  `p_city` varchar(255) NOT NULL,
  `p_address` varchar(255) NOT NULL,
  `p_eik` varchar(255) NOT NULL,
  `p_ddsnumber` varchar(255) DEFAULT NULL,
  `p_mol` varchar(255) NOT NULL,
  `p_bank` varchar(255) DEFAULT NULL,
  `p_iban` varchar(255) DEFAULT NULL,
  `p_bic` varchar(255) DEFAULT NULL,
  `p_zdds` tinyint(1) DEFAULT NULL,
  `author` varchar(255) NOT NULL,
  `author_user` int(10) unsigned NOT NULL,
  `author_sign` char(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `proforms_items`
--

DROP TABLE IF EXISTS `proforms_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proforms_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proform` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `quantity` decimal(12,4) unsigned NOT NULL,
  `mesurement` varchar(255) DEFAULT NULL,
  `price` decimal(13,4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'can_login','Активен потребител'),(2,'user_management','Управление на потребители'),(3,'invoices_settings','Настройки на фактурите'),(4,'global_settings','Общи настройки');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,1),(1,2),(1,3),(1,4),(2,1);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `realname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  `creation_session` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sign_prefix` char(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','d033e22ae348aeb5660fc2140aec35850c4da997','Администратор','2009-03-23 02:04:46','',1,0,'2012-02-06 11:42:05','-1','AD'),(2,'user','12dea96fec20593566ab75692c9949596833adc9','Потребител',NULL,'',0,0,'0000-00-00 00:00:00','','ПО');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-04-20 19:58:17
