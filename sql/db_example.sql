-- MySQL dump 10.13  Distrib 5.1.61, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: freeinfoice
-- ------------------------------------------------------
-- Server version	5.1.61-0ubuntu0.11.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contractors`
--

DROP TABLE IF EXISTS `contractors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `eik` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dds` tinyint(1) DEFAULT NULL,
  `ddsnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mol` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person` tinyint(1) DEFAULT '0',
  `egn` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractors`
--

LOCK TABLES `contractors` WRITE;
/*!40000 ALTER TABLE `contractors` DISABLE KEYS */;
INSERT INTO `contractors` VALUES (3,'„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932',1,'BG200345932','Димитър Иванов',0,NULL),(4,'„Нова тестова Фирма с малко по-дълго име“ ЕООД','София','ул. „Ботевско шосе“ №123','200322233',NULL,'','Спас Кировски',NULL,''),(7,'Димитър Касабов','София','ул. „Ботевско шосе“ №123','',NULL,'','',1,'6023445013'),(8,'„Тестова Фирма“ ООД','София','ул. „Ботевско шосе“ №123','344443123',NULL,'BG344443123','Иван Каров',NULL,''),(9,'Спас Киров Иванов','София','ул. „Ботевско шосе“ №123','',NULL,'','',1,'6023445012'),(14,'Иван Драсов','Варна','„Иван Драсов“ №12','',NULL,'','',1,'1234567890'),(13,'Димитър Пенков','гр. Смолян','бул. „България“ № 22','',NULL,'','',1,'0000000000');
/*!40000 ALTER TABLE `contractors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currency` char(3) NOT NULL,
  `rate` decimal(10,7) unsigned NOT NULL,
  `sign` char(5) NOT NULL,
  `longsign` varchar(255) NOT NULL,
  `subsign` char(5) NOT NULL,
  `default_c` tinyint(1) NOT NULL DEFAULT '0',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `g` char(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` VALUES (2,'EUR','1.9558300','€','евро','ц.',0,'2012-02-08 14:09:42','но,е'),(1,'BGN','1.0000000','лв.','лв.','ст.',1,'2012-03-12 11:45:19','ин,а');
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prefix` int(10) unsigned NOT NULL DEFAULT '0',
  `number` int(10) unsigned DEFAULT '0',
  `contractor` int(11) NOT NULL,
  `issue_date` date NOT NULL,
  `event_date` date NOT NULL,
  `receiver` varchar(255) NOT NULL,
  `bank_payment` int(10) unsigned NOT NULL DEFAULT '0',
  `vat` decimal(4,2) unsigned NOT NULL,
  `novatreason` varchar(255) DEFAULT NULL,
  `currency` int(11) NOT NULL,
  `rate` decimal(10,7) unsigned NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `related_invoice` varchar(255) DEFAULT NULL,
  `related_date` date DEFAULT NULL,
  `c_name` varchar(255) NOT NULL,
  `c_city` varchar(255) NOT NULL,
  `c_address` varchar(255) NOT NULL,
  `c_eik` varchar(255) DEFAULT NULL,
  `c_ddsnumber` varchar(255) DEFAULT NULL,
  `c_mol` varchar(255) DEFAULT NULL,
  `c_person` tinyint(1) DEFAULT NULL,
  `c_egn` varchar(255) DEFAULT NULL,
  `p_name` varchar(255) NOT NULL,
  `p_city` varchar(255) NOT NULL,
  `p_address` varchar(255) NOT NULL,
  `p_eik` varchar(255) NOT NULL,
  `p_ddsnumber` varchar(255) DEFAULT NULL,
  `p_mol` varchar(255) NOT NULL,
  `p_bank` varchar(255) DEFAULT NULL,
  `p_iban` varchar(255) DEFAULT NULL,
  `p_bic` varchar(255) DEFAULT NULL,
  `p_zdds` tinyint(1) DEFAULT NULL,
  `author` varchar(255) NOT NULL,
  `author_user` int(10) unsigned NOT NULL,
  `author_sign` char(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
INSERT INTO `invoices` VALUES (3,0,3,3,'2012-02-06','2012-02-06','Спас Митев',1,'0.00',NULL,1,'1.0000000',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',0,'Администратор',1,'AD'),(9,0,9,14,'2012-02-09','2012-02-09','Иван Драсов',1,'0.00',NULL,1,'0.0000000',0,'','0000-00-00','Иван Драсов','Варна','„Иван Драсов“ №12','','','',1,'1234567890','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',0,'Администратор',1,'AD'),(2,0,2,3,'2012-02-06','2012-02-05','Спас Митев',1,'20.00',NULL,1,'1.0000000',0,'0000000009','2012-02-09','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(4,0,4,3,'2012-02-08','2012-02-08','Спас Митев',1,'17.00',NULL,2,'1.9558300',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(5,0,5,7,'2012-02-08','2012-02-08','Спас Митев',1,'20.00',NULL,1,'1.9558300',0,'','0000-00-00','Димитър Касабов','София','ул. „Ботевско шосе“ №123','','','',1,'33333333','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(6,0,6,7,'2012-02-08','2012-02-08','Спас Митевски',1,'20.00',NULL,2,'1.9558300',0,'','0000-00-00','Димитър Касабов','Смолян','бул. „България“ № 22','','','',1,'6023445013','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(7,0,7,13,'2012-02-08','2012-02-08','Димитър Пенков',1,'20.00',NULL,1,'0.0000000',0,'','0000-00-00','Димитър Пенков','гр. Смолян','бул. „България“ № 22','','','',1,'0000000000','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(8,0,8,13,'2012-02-08','2012-02-08','Димитър Пенков',1,'0.00',NULL,1,'0.0000000',1,'0000000002','2012-02-06','Димитър Пенков','гр. Смолян','бул. „България“ № 22','','','',1,'0000000000','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',0,'Администратор',1,'AD'),(10,0,10,3,'2012-02-10','2012-02-10','Димитър Пенков',1,'20.00','Ми нямам пари',2,'1.9558300',0,'','2012-02-09','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(11,0,11,3,'2012-02-11','2012-02-11','Спас Митев',1,'20.00',NULL,1,'0.0000000',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(12,0,12,3,'2012-02-26','2012-02-26','Спас Митев',1,'20.00',NULL,1,'1.0000000',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(13,0,13,3,'2012-03-08','2012-03-08','Спас Митев',1,'20.00',NULL,2,'1.9558300',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(14,1,1,3,'2012-03-08','2012-03-08','Димитър Пенков',1,'20.00',NULL,1,'0.0000000',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(15,3,44,0,'0000-00-00','0000-00-00','',1,'0.00',NULL,0,'0.0000000',0,NULL,NULL,'','','',NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'',NULL,NULL,NULL,NULL,'Администратор',1,'AD'),(21,33,11,0,'0000-00-00','0000-00-00','',1,'0.00',NULL,0,'0.0000000',0,NULL,NULL,'','','',NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'',NULL,NULL,NULL,NULL,'Администратор',1,'AD'),(20,12,2,0,'0000-00-00','0000-00-00','',1,'0.00',NULL,0,'0.0000000',0,NULL,NULL,'','','',NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'',NULL,NULL,NULL,NULL,'Администратор',1,'AD'),(19,22,43,0,'0000-00-00','0000-00-00','',1,'0.00',NULL,0,'0.0000000',0,NULL,NULL,'','','',NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'',NULL,NULL,NULL,NULL,'Администратор',1,'AD'),(22,0,13,0,'0000-00-00','0000-00-00','',1,'0.00',NULL,0,'0.0000000',0,NULL,NULL,'','','',NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'',NULL,NULL,NULL,NULL,'Администратор',1,'AD'),(23,0,14,3,'2012-03-09','2012-03-09','Спас Митев',1,'20.00',NULL,1,'0.0000000',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(24,3,45,3,'2012-03-09','2012-03-09','Спас Митев',1,'20.00',NULL,1,'0.0000000',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(25,1,2,3,'2012-03-09','2012-03-09','Димитър Пенков',1,'20.00',NULL,1,'0.0000000',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(26,12,1,3,'2012-03-09','2012-03-09','Спас Митев',1,'20.00',NULL,1,'0.0000000',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(27,3,46,3,'2012-03-09','2012-03-09','Милена Самуилова',1,'20.00',NULL,1,'0.0000000',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Потребител',2,'ПО'),(28,3,47,4,'2012-03-09','2012-03-09','Димитър Пенков',1,'20.00',NULL,1,'0.0000000',0,'','0000-00-00','„Нова тестова Фирма с малко по-дълго име“ ЕООД','София','ул. „Ботевско шосе“ №123','200322233','','Спас Кировски',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Потребител',2,'ПО'),(29,12,2,3,'2012-03-09','2012-03-09','Спас Митев',0,'20.00',NULL,1,'0.0000000',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Потребител',2,'ПО'),(30,0,15,3,'2012-03-13','2012-03-13','Спас Митев',0,'7.00',NULL,1,'0.0000000',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ EООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(31,0,16,4,'2012-03-13','2012-03-13','Спас Митев',0,'0.00','Чл. 30 Ал. 1',1,'0.0000000',0,'','0000-00-00','„Нова тестова Фирма с малко по-дълго име“ ЕООД','София','ул. „Ботевско шосе“ №123','200322233','','Спас Кировски',NULL,'','„Тестова Фирма“ EООД','гр. София','ул. „Жива Вода“ № 1','200300400','','Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(32,2,25,0,'0000-00-00','0000-00-00','',0,'0.00',NULL,0,'0.0000000',0,NULL,NULL,'','','',NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'',NULL,NULL,NULL,NULL,'',0,''),(33,2,26,3,'2012-03-13','2012-03-13','Димитър Пенков',0,'20.00',NULL,1,'1.0000000',0,'','0000-00-00','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ EООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD');
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER |
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER invoice_numbering BEFORE INSERT ON invoices
  FOR EACH ROW BEGIN
       DECLARE next_id INT;
       DECLARE check_id INT;
       SET check_id = (SELECT MAX(number) FROM invoices WHERE prefix = NEW.prefix) + 1;
       IF check_id > 0 THEN SET next_id = check_id;
       ELSE SET next_id = 0;
       END IF;
       SET NEW.number = next_id;
  END */|
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `invoices_items`
--

DROP TABLE IF EXISTS `invoices_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `quantity` decimal(12,4) unsigned NOT NULL,
  `mesurement` varchar(255) DEFAULT NULL,
  `price` decimal(13,4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=233 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices_items`
--

LOCK TABLES `invoices_items` WRITE;
/*!40000 ALTER TABLE `invoices_items` DISABLE KEYS */;
INSERT INTO `invoices_items` VALUES (186,2,'сдафсфсд','23.0000','Други','23.0000'),(185,3,'Lenovo T400','1.0000','кг.','2350.0000'),(191,4,'Прясно мляко','3.0000','Други','1.2500'),(190,4,'Марули','2.0000','кг.','0.3000'),(189,4,'Краставици','1.0000','л.','2.4000'),(192,5,'Тестова стока','1.0000','кг.','2.4000'),(193,6,'Тестова стока','1.0000','кг.','2.4000'),(194,7,'Краставици','3.0000','кг.','0.2800'),(173,8,'Краставици','3.0000','кг.','0.2800'),(184,9,'Приспаднат аванс','1.0000','кг.','-200.0000'),(182,9,'Основното снегопочистване на улиците с извозване на сняг започва от 13 февруари, съгласно утвърдения графи. Призоват се гражданите също да окажат съдействие, премахвайки паркираните автомобили от вътреуличната мрежа.','1.0000','кг.','1.0000'),(183,9,'Желателно е да се премахнат и складираните дърва и други материали около улиците.','1.0000','кг.','1.0000'),(181,9,'В следващите три дни приоритетно ще се работи по сметоизвозването на града и населените места в общината.','1.0000','кг.','1.0000'),(180,9,'бщо 7 машини работят за разчистване и опесъчаване на общинската пътна мрежа в общината, уточниха още от фирмата. Тази нощ в региона отново падна сняг, образувайки покривка от 3-4 см. Пътната обстановка бавно започва да се подобрява.','1.0000','кг.','1.0000'),(176,9,'Phasellus pulvinar luctus enim ac placerat. Morbi non mi vitae tortor mattis interdum ut in ligula','6.0000','кг.','1.2000'),(177,9,'Авансово плащане за изработка на уебсайт за недвижими имоти и дизайн и изработка на touchscreen интерфейс към имотите','1.0000','кг.','1.0000'),(178,9,'Пътят Смолян – Мугла е пробит, но е отворен само за автомобили с висока проходимост. 26–километровото трасе бе почистено рано тази сутрин, като на този етап не се допуска преминаването на леки коли – с цел безопасност в участъка след Затворническото общеж','1.0000','кг.','1.0000'),(179,9,'Над 200 тона сняг и лед извозиха през нощта от централните зони на Смолян. Два багера и два самосвала на „Титан” и фирма „Красин” вече 24 часа премахват и извозват снега от градските улици, за да може почистването им да се улесни, а и за да поемат новата ','1.0000','кг.','1.0000'),(187,2,'Тестова стока','3.0000','кг.','22.0000'),(197,11,'Тестова стока','10.0000','кг.','1.6670'),(175,9,'Бира','3.0000','кг.','2.4000'),(174,9,'Краставици','23.0000','кг.','23.0000'),(195,10,'Тестова стока','22.0000','кг.','14.0000'),(196,10,'Бира','233.0000','кг.','23.0000'),(188,2,'вевwww','32.0000','л.','34.0000'),(209,12,'Бира','112.0000','кг.','2333.0000'),(208,12,'Тестова стока','133.0000','кг.','230.0000'),(210,12,'Таратанци','222.2330','кг.','23222.0000'),(211,13,'Тестова стока','22.0000','кг.','33.0000'),(213,14,'Дизайн и изработка на рекламни материали','2.0000','кг.','20.0000'),(214,23,'Тестова стока','1.0000','кг.','1.0000'),(215,24,'Дизайн и изработка на рекламни материали','22.0000','кг.','22.0000'),(216,25,'Краставици','34.0000','кг.','23.0000'),(217,26,'Краставици','223.0000','кг.','23.0000'),(218,27,'Изработка на уебсайт','1.0000','кг.','20.0000'),(219,28,'Краставици','10.0000','кг.','20.0000'),(227,29,'Дизайн и изработка на рекламни материали','1.0000','бр.','1.0000'),(228,30,'Изработка на уебсайт','1.0000','бр.','500.0000'),(229,31,'Изработка на уебсайт','1.0000','бр.','600.0000'),(231,33,'Тестова стока','1.0000','бр.','2.0000'),(232,33,'Бира','22.0000','бр.','25.8333');
/*!40000 ALTER TABLE `invoices_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_methods`
--

DROP TABLE IF EXISTS `payment_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_methods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_methods`
--

LOCK TABLES `payment_methods` WRITE;
/*!40000 ALTER TABLE `payment_methods` DISABLE KEYS */;
INSERT INTO `payment_methods` VALUES (0,'В брой'),(1,'По банков път'),(6,'С насрещно прихващане'),(5,'Чек / Ваучер'),(4,'Платежно нареждане'),(3,'С карта'),(2,'Наложен платеж'),(7,'Други методи');
/*!40000 ALTER TABLE `payment_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proforms`
--

DROP TABLE IF EXISTS `proforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proforms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contractor` int(11) NOT NULL,
  `issue_date` date NOT NULL,
  `bank_payment` int(11) NOT NULL DEFAULT '0',
  `vat` decimal(4,2) unsigned NOT NULL,
  `novatreason` varchar(255) DEFAULT NULL,
  `currency` int(11) NOT NULL,
  `rate` decimal(10,7) unsigned NOT NULL,
  `c_name` varchar(255) NOT NULL,
  `c_city` varchar(255) NOT NULL,
  `c_address` varchar(255) NOT NULL,
  `c_eik` varchar(255) DEFAULT NULL,
  `c_ddsnumber` varchar(255) DEFAULT NULL,
  `c_mol` varchar(255) DEFAULT NULL,
  `c_person` tinyint(1) DEFAULT NULL,
  `c_egn` varchar(255) DEFAULT NULL,
  `p_name` varchar(255) NOT NULL,
  `p_city` varchar(255) NOT NULL,
  `p_address` varchar(255) NOT NULL,
  `p_eik` varchar(255) NOT NULL,
  `p_ddsnumber` varchar(255) DEFAULT NULL,
  `p_mol` varchar(255) NOT NULL,
  `p_bank` varchar(255) DEFAULT NULL,
  `p_iban` varchar(255) DEFAULT NULL,
  `p_bic` varchar(255) DEFAULT NULL,
  `p_zdds` tinyint(1) DEFAULT NULL,
  `author` varchar(255) NOT NULL,
  `author_user` int(10) unsigned NOT NULL,
  `author_sign` char(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proforms`
--

LOCK TABLES `proforms` WRITE;
/*!40000 ALTER TABLE `proforms` DISABLE KEYS */;
INSERT INTO `proforms` VALUES (1,3,'2012-02-10',1,'0.00',NULL,2,'1.9558300','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',0,'Администратор',1,'AD'),(2,3,'2012-02-10',1,'20.00',NULL,1,'1.0000000','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Администратор',1,'AD'),(3,3,'2012-03-09',0,'20.00',NULL,1,'1.0000000','„Тестова Фирма“ ООД','Смолян','ул „Соколица“ № 28','200345932','BG200345932','Димитър Иванов',NULL,'','„Тестова Фирма“ ЕООД','гр. София','ул. „Жива Вода“ № 1','200300400',NULL,'Джон Смит','Първа Инвестиционна Банка - клон София','BG99 FINV 9999 9999 9999 99','FINVBGSF',1,'Потребител',2,'ПО');
/*!40000 ALTER TABLE `proforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proforms_items`
--

DROP TABLE IF EXISTS `proforms_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proforms_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proform` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `quantity` decimal(12,4) unsigned NOT NULL,
  `mesurement` varchar(255) DEFAULT NULL,
  `price` decimal(13,4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proforms_items`
--

LOCK TABLES `proforms_items` WRITE;
/*!40000 ALTER TABLE `proforms_items` DISABLE KEYS */;
INSERT INTO `proforms_items` VALUES (25,1,'Тестова стока','1.0000','кг.','22.0000'),(31,2,'Прясно мляко','233.0000','кг.','0.8330'),(30,2,'Бира','3.0000','кг.','1.6670'),(29,2,'Тестова стока','1.0000','кг.','20.0000'),(33,3,'Дизайн и изработка на рекламни материали','1.0000','кг.','22.0000');
/*!40000 ALTER TABLE `proforms_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'can_login','Активен потребител'),(2,'user_management','Управление на потребители'),(3,'invoices_settings','Настройки на фактурите'),(4,'global_settings','Общи настройки');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,1),(1,2),(1,3),(1,4),(2,1);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `realname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  `creation_session` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sign_prefix` char(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','d033e22ae348aeb5660fc2140aec35850c4da997','Администратор','2009-03-23 02:04:46','',1,0,'2012-02-06 11:42:05','-1','AD'),(2,'user','12dea96fec20593566ab75692c9949596833adc9','Потребител',NULL,'',0,0,'0000-00-00 00:00:00','','ПО');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-04-20 19:58:17
