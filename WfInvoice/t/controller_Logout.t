use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'WfInvoice' }
BEGIN { use_ok 'WfInvoice::Controller::Logout' }

# ok(request('/logout')->is_success, 'Request should succeed');
done_testing();
