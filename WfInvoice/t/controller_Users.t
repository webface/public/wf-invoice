use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'WfInvoice' }
BEGIN { use_ok 'WfInvoice::Controller::Users' }

# ok(request('/users')->is_success, 'Request should succeed');
done_testing();
