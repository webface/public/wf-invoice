use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'WfInvoice' }
BEGIN { use_ok 'WfInvoice::Controller::Invoices::Export' }

ok(request('/invoices/export')->is_success, 'Request should succeed');
done_testing();
