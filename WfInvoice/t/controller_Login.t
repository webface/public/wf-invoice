use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'WfInvoice' }
BEGIN { use_ok 'WfInvoice::Controller::Login' }

ok(request('/login')->is_success, 'Request should succeed');
done_testing();
