var formcheckLanguage = {
    required: "Това поле е задължително.",
    alpha: "Това поле приема само букви.",
    alphanum: "Това поле приема само букви и цифри.",
    nodigit: "Не се приемат цифри.",
    digit: "Моля въведете валидно число.",
    digitmin: "Числото трябва да е поне %0",
    digitltd: "Стойността трябва да е между %0 и %1",
    number: "Моля въведете валидно число.",
    email: "Моля въведете валиден пощенски адрес: <br /><span>Пример: yourname@domain.com</span>",
    phone: "Моля въведете валиден телефон.",
    url: "Моля въведете валиден уеб адрес: <br /><span>Пример: http://www.domain.com</span>",

    confirm: "Полетата се различават",
    differs: "Това поле трябва да е с различна стойност",
    length_str: "Невалидна дължина, трябва да е между %0 и %1 символа",
    lengthmax: "Невалидна дължина, трябва де е максимум %0 символа",
    lengthmin: "Невалидна дължина, трябва да е поне %0 символа",
    checkbox: "Моля отметнете",
    radios: "Моля изберете",
    select: "Моля изберете стойност",
    length_fix: "Това поле трябва да е с дължина точно %0 символа"
};