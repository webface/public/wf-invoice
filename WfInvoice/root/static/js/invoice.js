/*  This file is part of WfInvoice -- the free invoicing software            */
/*  Copyright (C) 2012 Anton Katsarov <anton@webface.bg>		     */
/*  									     */
/*  This program is free software: you can redistribute it and/or modify     */
/*  it under the terms of the GNU Affero General Public License as	     */
/*  published by the Free Software Foundation, either version 3 of the	     */
/*  License, or (at your option) any later version.			     */
/*  									     */
/*  This program is distributed in the hope that it will be useful,	     */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	     */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the	     */
/*  GNU Affero General Public License for more details.			     */
/*  									     */
/*  You should have received a copy of the GNU Affero General Public License */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.    */

window.addEvent("domready", function () {
    $('add_row').addEvent("click", function (e) {
	e.stop();
	new Request.HTML({
	    url: '/invoices/add_row?row=' + $('rows').get("value"),
	    onSuccess:function (responseTrere, responseElements, responseHTML ) {
		var tr = new Element("tr");
		tr.set("html", responseHTML);
		tr.inject($("tbody"));
		$('rows').set("value", $('rows').get("value").toInt() + 1);
		new initTr(tr);
		updateCurrency();
	    }
	}).get();
    });
    $$("#tbody tr").each(function (tr) {
	new initTr(tr);
    });
    new autoFill("c_name","/contractors/fill", {inputPrefix: "c_"});
    new autoFill("c_eik","/contractors/fill", {inputPrefix: "c_"});
    new autoFill("c_egn","/contractors/fill", {inputPrefix: "c_"});
    if ($('related_invoice')) new autoFill("related_invoice","/invoices/fill", {minChars: 1});
    $$("table input").each( function (inp) {inp.fireEvent("change")});

    $("currency").addEvent("change", function () {
	updateCurrency();
    });
    updateCurrency(true);
    if ($("type1")) {
	$$("#type1, #type2, #type3").each(function (radio) {
	    radio.addEvent("change", function () {
		updateType();
	    });
	});
	updateType();
    }
});

var updateType = function () {
    if ($("type1").get("checked")) {
	$$(".notice").setStyle("display","none");
	formcheck.dispose($('related_invoice'));
	formcheck.dispose($('rdate'));
    } else {
	$$(".notice").setStyle("display","block");
	formcheck.register($('related_invoice'));
	formcheck.register($('rdate'));
    };
};

var updateCurrency = function (disableCnahge) {
    var cId = $("currency").get("value");
    var cRate =  $("currency").getElement("[value=" + cId + "]").get("label");
    var cText = $("currency").getElement("[value=" + cId + "]").get("text").split("-")[1];
    if (!disableCnahge) {
	$("rate").set("value", cRate);
    }
	$$(".currency_sign").each(function(span) {
	    span.set("text", cText);
	});
    if (cRate == 1) {
	$$(".rateNote").setStyle("display","none");
    } else {
	$$(".rateNote").setStyle("display","inline");
    }
};

var initTr = new Class({
    Implements: [Options],
    options: {
	classname: "autofill",
	inputPrefix: ""
    },
    initialize: function(tr, options){
	this.qty = tr.getElement("input.qty");
	this.inputToFolat(this.qty, lQuantity);
	this.price = tr.getElement("input.price");
	this.inputToFolat(this.price, lSprice);
	this.total = tr.getElement("span.total");
	this.remove = tr.getElement("a.remove");
	this.remove.addEvent("click", function (e) {
	    e.stop();
	    if ($$("table tr td a.remove").length > 1) {
		this.qty.set("value","0");
		tr.destroy();
	    }
	}.bind(this));
    },
    inputToFolat: function (input, digs) {
	input.addEvent("change", function () {
	    var text = input.get("value");
	    text = text.replace(/,/g, ".");
	    var value  = "";
	    if (text.toFloat()) value =  sprintf('%.' + digs + 'f', text.toFloat());
	    input.set("value", value);
	    this.total.set("text", sprintf('%.' + lPrice + 'f', (this.qty.get('value') * this.price.get('value')) )) ;
	    this.calcTotal();
	}.bind(this));
	if ($("vat")) {
	    $("vat").addEvent("change", function () {
		this.calcTotal();
	    }.bind(this));
	}
    },
    calcTotal: function (string) {
	var total = 0;
	$$("#tbody span.total").each(function (span) {
	    total = total + span.get("text").toFloat();
	});
	if ($("precalc")) {
	    var vat = $("vat").get("value").toFloat()/100 + 1;
	    if ($("precalc").get("value") == 1) {
		$("total").set("text",sprintf('%.' + lPrice + 'f', total));
		$("total_no_dds").set("text",sprintf('%.' + lPrice + 'f', (total/vat)));
		$("added_dds").set("text",sprintf('%.' + lPrice + 'f', ((vat - 1) * total / vat ) ));
	    } else {
		$("total_no_dds").set("text",sprintf('%.' + lPrice + 'f', total));
		$("added_dds").set("text",sprintf('%.' + lPrice + 'f', (total * (vat - 1) ) ));
		$("total").set("text",sprintf('%.' + lPrice + 'f', (total * vat)));
	    }
	} else {
	    $("total").set("text",sprintf('%.' + lPrice + 'f', total));
	}
    }
});
var autoFill = new Class({
    Implements: [Options],
    options: {
	classname: "autofill",
	inputPrefix: "",
	minChars: 3
    },
    initialize: function(input, url, options){
	this.setOptions(options);
	this.input = $(input);
	this.url = url;

	this.holder = new Element("ul", {
	    "class": this.options.classname + "-holder"
	});
	this.holder.inject(document.body);
	this.holder.setStyles({
	    "top" : (this.input.getPosition().y + this.input.getSize().y),
	    "left" : this.input.getPosition().x,
	    "min-width" : this.input.getSize().x,
	    "display": "none"
	});

	this.input.addEvent("keyup", function () {
	    if (this.input.get("value").length >= this.options.minChars) {
		new Request.JSON({
		    url: this.url + "?string=" + this.input.get("value"),
		    onSuccess:function (json, string) {
			this.holder.empty();
			json.each(function (item) {
			    var itemDOM = this.genItem(item);
			}.bind(this));
			this.holder.setStyle("display","block");

		    }.bind(this),
		    onError: function (a, e) {
			alert(e);
		    }
 		}).get();
	    }
	}.bind(this));
    },
    genItem: function (item) {
	var itemDOM = new Element("li").set("text",item.name);

	itemDOM.addEvent("click", function () {

		new Request.JSON({
		    url: this.url + "?id=" + item.id,
		    onSuccess:function (json) {
			var item = new Hash(json);
			item.each(function (value,key) {
			    if ($(this.options.inputPrefix + key).get('type') == "checkbox") {
				if (value == 1) {
				    $(this.options.inputPrefix + key).checked = true;
				} else {
				    $(this.options.inputPrefix + key).checked = false;
				}
				$(this.options.inputPrefix + key).fireEvent("change");
			    } else {
				$(this.options.inputPrefix + key).set("value", value);
			    }
			}.bind(this));
			this.holder.setStyle("display","none");

		    }.bind(this),
		    onError: function (a) {
			alert(a);
		    }
 		}).get();

	}.bind(this));

	itemDOM.inject(this.holder);
    }
});