/*  This file is part of WfInvoice -- the free invoicing software            */
/*  Copyright (C) 2012 Anton Katsarov <anton@webface.bg>		     */
/*  									     */
/*  This program is free software: you can redistribute it and/or modify     */
/*  it under the terms of the GNU Affero General Public License as	     */
/*  published by the Free Software Foundation, either version 3 of the	     */
/*  License, or (at your option) any later version.			     */
/*  									     */
/*  This program is distributed in the hope that it will be useful,	     */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	     */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the	     */
/*  GNU Affero General Public License for more details.			     */
/*  									     */
/*  You should have received a copy of the GNU Affero General Public License */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.    */

window.addEvent("domready", function () {
    updateVisibility();
    $("c_person").addEvent("change", function () {
	updateVisibility();
    });
});

var updateVisibility = function () {
    if ($("c_person").get("checked")) {
	$$(".person").setStyle("display","block");
	$$(".company").setStyle("display","none");
 	formcheck.register($('c_egn'));
	formcheck.dispose($('c_eik'));
	formcheck.dispose($('c_mol'));

    } else {
	$$(".person").setStyle("display","none");
	$$(".company").setStyle("display","block");
 	formcheck.dispose($('c_egn'));
	formcheck.register($('c_eik'));
	formcheck.register($('c_mol'));
    }
};