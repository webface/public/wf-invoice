package WfInvoice::Controller::Contractors;
use utf8;

=head1 LICENSE

    This file is part of WfInvoice -- the free invoicing software
    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use Moose;
use namespace::autoclean;
use JSON;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

WfInvoice::Controller::Contractors - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index : Path : Args(0) {
  my ($self, $c) = @_;
  my $page = 1;
  if ($c->request->args->[0]) {
    $page = $c->request->args->[0];
    $c->response->redirect($c->uri_for('/contractors')) if $page == 1;
  }
  $c->stash->{contractors} = [
    $c->model($c->config->{name} . "DB::Contractors")->search(
      {},
      { order_by => ['me.name ASC'],
        rows     => $c->config->{rows_per_page},
        page     => $page
      }
      )->all()
  ];
  my $contractorscount = $c->model($c->config->{name} . "DB::Contractors")->search(
    {},
    { select => [
		 \"COUNT(me.id)"
      ],
      as => [
        qw/
          count
          /
      ]
    }
  )->first()->get_column('count');

  $c->forward($c->config->{name} . "::Controller::Root",
    'pager',
    [$contractorscount, $page, $c->config->{rows_per_page}, '/contractors/page']);
  return;
}

sub page : LocalRegex('page\-(\d+)$') {
  my ($self, $c) = @_;
  $c->forward($c->action->class, 'index', [$c->request->captures->[0]]);
  $c->stash->{template} = "contractors/index.tt2";
  return;
}

sub fill : Local {
  my ($self, $c) = @_;

  if ($c->request->params->{string}) {
    my $contractors = [
      $c->model($c->config->{name} . "DB::Contractors")->search(
        [ name      => {"LIKE" => "%" . $c->request->params->{string} . "%"},
          eik       => {"LIKE" => "%" . $c->request->params->{string} . "%"},
          egn       => {"LIKE" => "%" . $c->request->params->{string} . "%"},
          ddsnumber => {"LIKE" => "%" . $c->request->params->{string} . "%"},
        ],
        { order_by => ['me.id ASC'],
          rows     => 20
        }
        )->all()
    ];
    my $items = [];
    for (@$contractors) {
      my $values = {
        name => $_->name . " / " . $_->eik . $_->egn . " - " . $_->city,
        id   => $_->id
      };
      push @$items, $values;
    }
    $c->response->body(to_json($items));
  }
  elsif ($c->request->params->{id}) {
    my $contractor = $c->model($c->config->{name} . "DB::Contractors")->search(
      {id => $c->request->params->{id}},
      { order_by => ['me.id ASC'],
        rows     => 1
      }
    )->first();
    my $item = {
      name      => $contractor->name,
      id        => $contractor->id,
      city      => $contractor->city,
      address   => $contractor->address,
      eik       => $contractor->eik,
      ddsnumber => $contractor->ddsnumber,
      mol       => $contractor->mol,
      person    => $contractor->person,
      egn       => $contractor->egn,
    };
    $c->response->body(to_json($item));
  }
  else {
    $c->detach("/default");
  }
  return;
}


sub add : Local {
  my ($self, $c) = @_;
  my ($id) = @{$c->request->args};
  if ($c->request->method() eq "POST") {
    $c->stash->{errors} = $c->form(required => [qw( name city address )],);
    unless ($c->stash->{errors}->has_missing or $c->stash->{errors}->has_invalid) {
      my $query = {
        name      => $c->request->params->{name},
        city      => $c->request->params->{city},
        address   => $c->request->params->{address},
        eik       => $c->request->params->{eik},
        ddsnumber => $c->request->params->{ddsnumber},
        mol       => $c->request->params->{mol},
        person    => $c->request->params->{person},
        egn       => $c->request->params->{egn},
      };
      my $item;
      if ($id) {
        $item =
          $c->model($c->config->{name} . "DB::Contractors")->search({id => $id})
          ->first();
        $item->update($query);
      }
      else {
        $item = $c->model($c->config->{name} . "DB::Contractors")->create($query);
      }


    }
  }
  return;
}

sub edit : LocalRegex("^edit/(\d+)$") {
  my ($self, $c) = @_;
  my $data;
  $c->forward($c->action->class, 'add', [$c->request->snippets->[0]]);

  my $item =
    $c->model($c->config->{name} . "DB::Contractors")
    ->search({id => $c->request->snippets->[0]})->first();
  for (@{[$c->model($c->config->{name} . "DB::Contractors")->result_source->columns]}) {
    $data->{$_} = $item->get_column($_);
  }

  $c->stash->{template} = "contractors/add.tt2";
  $c->forward($c->view('TT'));
  $c->fillform($data);
  return;
}

__PACKAGE__->meta->make_immutable;

1;
