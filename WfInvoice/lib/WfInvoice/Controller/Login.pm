package WfInvoice::Controller::Login;
use utf8;

=head1 LICENSE

    This file is part of WfInvoice -- the free invoicing software
    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

WfInvoice::Controller::Login - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

=head2 index

=cut

sub index : Path : Args(0) {
  my ($self, $c) = @_;

  # Get the username and password from form
  my $username = $c->request->params->{username} || "";
  my $password = $c->request->params->{password} || "";

  # If the username and password values were found in form
  if ($username && $password) {

    # Attempt to log the user in
    if (
      $c->authenticate(
        { username => $username,
          password => $password
        }
      )
      )
    {

      # If successful, then let them use the application
      if ($c->check_user_roles('can_login')) {
        $c->response->redirect($c->uri_for('/'));
      }
    }
  }
  return;
}

__PACKAGE__->meta->make_immutable;

1;
