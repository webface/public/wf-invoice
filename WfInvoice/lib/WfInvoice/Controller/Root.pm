package WfInvoice::Controller::Root;
use utf8;

=head1 LICENSE

    This file is part of WfInvoice -- the free invoicing software
    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=head1 NAME

WfInvoice::Controller::Root - Root Controller for WfInvoice

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 index

The root page (/)

=cut

sub begin : Private {
  my ($self, $c) = @_;
  if (!$c->user_exists) {
    $c->stash->{template} = "login/index.tt2";
    $c->forward($c->view("Ajax"));
  }
  return;
}

sub index : Path : Args(0) {
  my ($self, $c) = @_;
  $c->stash->{template} = "index.tt2";
  return;
}

sub pager : Private {
  my ($self, $c) = @_;
  my ($count, $page, $rows, $base_url) = @{$c->request->args};
  $c->stash->{pager}->{count}    = $count;
  $c->stash->{pager}->{rows}     = $rows;
  $c->stash->{pager}->{page}     = $page;
  $c->stash->{pager}->{base_url} = $base_url;

  my $add = ($count % $rows) ? 1 : 0;
  $c->stash->{pager}->{pages} = int($count / $rows) + $add;

  $c->stash->{pager}->{half_count} = int(($c->config->{max_pager_elemnts} - 6) / 2);

  $c->stash->{pager}->{index1} = 0;
  if (($page - 1) <= $c->stash->{pager}->{half_count}) {
    $c->stash->{pager}->{index1} = $c->stash->{pager}->{half_count} - $page + 3;
  }
  if ($page == 1) {
    $c->stash->{pager}->{index1} = $c->stash->{pager}->{half_count} + 3;
  }

  $c->stash->{pager}->{index2} = 0;
  if ($page >= $c->stash->{pager}->{pages} - $c->stash->{pager}->{half_count}) {
    $c->stash->{pager}->{index2} =
      $c->stash->{pager}->{pages} - $c->stash->{pager}->{half_count} - $page - 2;
  }
  return;
}

sub changepassword : Local {
  my ($self, $c) = @_;
  if ($c->request->method() eq "POST") {
    my $query = {
      realname    => $c->request->params->{'realname'},
      sign_prefix => $c->request->params->{'sign_prefix'},
    };
    if ($c->request->params->{pass}) {
      $query->{password} = unpack("H*", sha1($c->request->param('pass')));
    }
    $c->model($c->config->{name} . "DB::Users")->search({id => $c->user->id})
      ->update($query);

    $c->response->redirect($c->uri_for("/changepassword"));
  }
  my $data;
  $data->{realname}    = $c->user->realname;
  $data->{sign_prefix} = $c->user->sign_prefix;
  $c->forward($c->view('TT'));
  $c->fillform($data);
  return;
}

=head2 default

Standard 404 error page

=cut

sub default : Path {
  my ($self, $c) = @_;
  $c->response->body('Page not found');
  $c->response->status(404);
  return;
}

=head2 end

Attempt to render a view, if needed.

=cut

sub end : ActionClass('RenderView') {
  my ($self, $c) = @_;
  if ($c->error && defined($c->error->[-1])) {
    if ($c->error->[-1] =~ m#DBD::mysql::st\ execute\ failed:\ Duplicate entry#x) {
      $c->error(0);
      $c->response->status(409);
      $c->stash->{duplicate} = 1;
      $c->forward($c->view('TT'));
      $c->fillform($c->request->params);
    }
    else {    # all other errors (uncomment the following lines to enable)
      $c->stash->{error} = $c->error->[-1];
      $c->error(0);
      $c->response->status(500);
      $c->stash->{template} = 'error.tt2';
    }
  }
  return;
}

__PACKAGE__->meta->make_immutable;

1;
