package WfInvoice::Controller::Invoices;
use utf8;

=head1 LICENSE

    This file is part of WfInvoice -- the free invoicing software
    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use Moose;
use namespace::autoclean;
use PDF::Reuse;
use JSON;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

WfInvoice::Controller::Invoices - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

=head2 index

=cut

sub index : Path : Args(0) {
  my ($self, $c) = @_;
  my $page = 1;
  if ($c->request->args->[0]) {
    $page = $c->request->args->[0];
    $c->response->redirect($c->uri_for('/invoices')) if $page == 1;
  }
  $c->stash->{invoices} = [
    $c->model($c->config->{name} . "DB::Invoices")->search(
      {},
      { join    => ["items", "currency"],
        +select => [
          "me.id", "me.c_name", "me.issue_date", "currency.sign",
          "SUM(items.price * items.quantity * (1 + me.vat/100) )",
          "me.prefix", "me.number"
        ],
        +as => [
          qw/
            id
            c_name
            issue_date
            curr
            value
            prefix
            number
            /
        ],
        order_by => ['me.id DESC'],
        group_by => ["me.id"],
        rows     => $c->config->{rows_per_page},
        page     => $page
      }
      )->all()
  ];
  my $invoicescount = $c->model($c->config->{name} . "DB::Invoices")->search(
    {contractor => {">" => 0}},
    { select => [
		 \"COUNT(me.id)"
      ],
      as => [
        qw/
          count
          /
      ]
    }
  )->first()->get_column('count');

  $c->forward($c->config->{name} . "::Controller::Root",
    'pager', [$invoicescount, $page, $c->config->{rows_per_page}, '/invoices/page']);

  return;
}

sub page : LocalRegex('page\-(\d+)$') {
  my ($self, $c) = @_;
  $c->forward($c->action->class, 'index', [$c->request->captures->[0]]);
  $c->stash->{template} = "invoices/index.tt2";
  return;
}


sub add : Local {
  my ($self, $c) = @_;
  my ($id) = @{$c->request->args};
  if ($c->action->name eq "add") {
    $c->stash->{prefixes} =
      [$c->model($c->config->{name} . "DB::InvoicesPrefixes")
        ->search({}, {bind => [$c->user->id]})->all()
      ];
  }
  $c->stash->{currencies} =
    [$c->model($c->config->{name} . "DB::Currencies")
      ->search({}, {order_by => ["default_c DESC", "id ASC"]})->all()
    ];
  if ($c->request->method() eq "POST") {
    $c->stash->{errors} = $c->form(
      required => [qw( c_name c_city c_address receiver name_1 quantity_1 price_1 )],);
    unless ($c->stash->{errors}->has_missing or $c->stash->{errors}->has_invalid) {
      my $cquery = {
        name      => $c->request->params->{c_name},
        city      => $c->request->params->{c_city},
        address   => $c->request->params->{c_address},
        eik       => $c->request->params->{c_eik},
        ddsnumber => $c->request->params->{c_ddsnumber},
        mol       => $c->request->params->{c_mol},
        person    => $c->request->params->{c_person},
        egn       => $c->request->params->{c_egn},
      };
      if ($c->request->params->{contractor}) {
        $cquery->{id} = $c->request->params->{contractor};
      }
      my $contractor =
        $c->model($c->config->{name} . "DB::Contractors")->find_or_create($cquery);
      my $issue = $c->request->params->{issue_date} || \"NOW()";
      my $query = {
        contractor   => $contractor->id,
        issue_date   => $issue,
        event_date   => ($c->request->params->{event_date} || $issue),
        receiver     => $c->request->params->{receiver},
        bank_payment => $c->request->params->{bank_payment},
        c_name       => $c->request->params->{c_name},
        c_city       => $c->request->params->{c_city},
        c_address    => $c->request->params->{c_address},
        c_eik        => $c->request->params->{c_eik},
        c_ddsnumber  => $c->request->params->{c_ddsnumber},
        c_mol        => $c->request->params->{c_mol},
        c_person     => $c->request->params->{c_person},
        c_egn        => $c->request->params->{c_egn},
        p_name => ($c->request->params->{p_name} || $c->config->{company}->{name}),
        p_city => ($c->request->params->{p_city} || $c->config->{company}->{city}),
        p_address =>
          ($c->request->params->{p_address} || $c->config->{company}->{address}),
        p_eik => ($c->request->params->{p_eik} || $c->config->{company}->{eik}),
        p_ddsnumber =>
          ($c->request->params->{p_ddsnumber} || $c->config->{company}->{dds}),
        p_mol => ($c->request->params->{p_mol} || $c->config->{company}->{mol}),
        p_bank => (
          $c->request->params->{p_bank} || $c->config->{company}->{bank_account}->{bank}
        ),
        p_iban => (
          $c->request->params->{p_iban} || $c->config->{company}->{bank_account}->{iban}
        ),
        p_bic => (
          $c->request->params->{p_bic} || $c->config->{company}->{bank_account}->{bic}
        ),
        p_zdds => (
          (defined($c->request->params->{p_zdds}))
          ? $c->request->params->{p_zdds}
          : (($c->config->{vat} > 1) ? 1 : 0)
        ),
        currency        => $c->request->params->{currency},
        rate            => $c->request->params->{rate},
        type            => $c->request->params->{type},
        related_invoice => $c->request->params->{related_invoice},
        related_date    => $c->request->params->{related_date},
        vat             => ($c->request->params->{vat} || 0),
        novatreason     => ($c->request->params->{novatreason} || undef),
      };
      my $item;
      if ($id) {
        $item =
          $c->model($c->config->{name} . "DB::Invoices")->search({id => $id})->first();
        $item->update($query);
        $c->model($c->config->{name} . "DB::InvoicesItems")
          ->search({invoice => $item->id})->delete();
      }
      else {
        $query->{prefix}      = $c->request->params->{prefix};
        $query->{author}      = $c->user->realname;
        $query->{author_user} = $c->user->id;
        $query->{author_sign} = $c->user->sign_prefix;
        $item = $c->model($c->config->{name} . "DB::Invoices")->create($query);
      }

      for (1 .. $c->request->params->{rows}) {
        if ( $c->request->params->{'name_' . $_}
          && $c->request->params->{'quantity_' . $_}
          && $c->request->params->{'mesurement_' . $_}
          && $c->request->params->{'price_' . $_})
        {
          my $price = 0;
          if ($c->config->{vat_precalc}) {
            $price = $c->request->params->{'price_' . $_} / (1 + $item->vat / 100);
          }
          else {
            $price = $c->request->params->{'price_' . $_};
          }
          $c->model($c->config->{name} . "DB::InvoicesItems")->create(
            { invoice    => $item->id,
              name       => $c->request->params->{'name_' . $_},
              quantity   => $c->request->params->{'quantity_' . $_},
              mesurement => $c->request->params->{'mesurement_' . $_},
              price      => $price
            }
          );
        }
      }
      $c->response->redirect($c->uri_for("/invoices"));
    }
  }
  $c->stash->{payment_methods} =
    [$c->model($c->config->{name} . "DB::PaymentMethods")
      ->search({}, {order_by => "me.id ASC"})->all()
    ];
  return;
}

sub fill : Local {
  my ($self, $c) = @_;

  if ($c->request->params->{string}) {
    my $invoices = [
      $c->model($c->config->{name} . "DB::Invoices")->search(
        {'me.number' => {"LIKE" => "%" . int($c->request->params->{string}) . "%"},},
        { join    => ["items", "currency"],
          +select => [
            "me.id",
            "me.c_name",
            "me.issue_date",
            "currency.sign",
            "SUM(items.price * (1 + me.vat/100 * "
              . int(!$c->config->{vat_precalc}) . "))",
            "me.prefix",
            "me.number"
          ],
          +as => [
            qw/
              id
              c_name
              issue_date
              curr
              value
              prefix
              number
              /
          ],
          order_by => ['me.id ASC'],
          group_by => ["me.id"],
          rows     => 20,
        }
        )->all()
    ];
    my $items = [];
    for (@$invoices) {
      my $values = {
        name => sprintf("%02s", $_->get_column("prefix"))
          . sprintf("%08s", $_->get_column("number")) . " / "
          . $_->get_column("c_name") . " / "
          . $_->get_column("issue_date") . " / "
          . sprintf("%.2f", $_->get_column("value"))
          . $_->get_column("curr"),
        id => $_->get_column("id")
      };
      push @$items, $values;
    }
    $c->response->body(to_json($items));
  }
  elsif ($c->request->params->{id}) {
    my $invoice = $c->model($c->config->{name} . "DB::Invoices")->search(
      {id => $c->request->params->{id}},
      { order_by => ['me.id ASC'],
        rows     => 1
      }
    )->first();
    my $item = {
      rdate           => $invoice->issue_date,
      related_invoice => sprintf("%02s", $invoice->prefix)
        . sprintf("%08s", $invoice->number),
    };
    $c->response->body(to_json($item));
  }
  else {
    $c->detach("/default");
  }
  return;
}


sub edit : LocalRegex("^edit/(\d+)$") {
  my ($self, $c) = @_;
  my $data;
  $c->forward($c->action->class, 'add', [$c->request->snippets->[0]]);

  my $item =
    $c->model($c->config->{name} . "DB::Invoices")
    ->search({id => $c->request->snippets->[0]})->first();
  for (@{[$c->model($c->config->{name} . "DB::Invoices")->result_source->columns]}) {
    $data->{$_} = $item->get_column($_);
  }
  my $contr =
    $c->model($c->config->{name} . "DB::Contractors")
    ->search({id => $item->get_column("contractor")})->first();
  for (@{[$c->model($c->config->{name} . "DB::Contractors")->result_source->columns]}) {
    $data->{$_} = $contr->get_column($_);
  }

  my $items =
    [$c->model($c->config->{name} . "DB::InvoicesItems")
      ->search({invoice => $c->request->snippets->[0]}, {order_by => ["me.id ASC"]})
      ->all()
    ];
  my $count = 0;
  for (@$items) {
    $count++;
    $data->{'name_' . $count}       = $_->get_column('name');
    $data->{'quantity_' . $count}   = $_->get_column('quantity');
    $data->{'mesurement_' . $count} = $_->get_column('mesurement');
    if ($c->config->{vat_precalc}) {
      $data->{'price_' . $count} = $_->get_column('price') * (1 + $item->vat / 100);
    }
    else {
      $data->{'price_' . $count} = $_->get_column('price');
    }
  }
  $c->stash->{count}    = $count;
  $c->stash->{url}      = $item->id;
  $c->stash->{number}   = $item->number;
  $c->stash->{prefix}   = $item->prefix;
  $c->stash->{zdds}     = $item->p_zdds;
  $c->stash->{template} = "invoices/add.tt2";
  $c->forward($c->view('TT'));
  $c->fillform($data);
  return;
}

sub add_row : Local {
  my ($self, $c) = @_;
  $c->forward($c->view("Ajax"));
  return;
}

sub get_pdf : LocalRegex("(original|copy)/(\d+)$") {
  my ($self, $c) = @_;

  $c->stash->{inv} =
    $c->model($c->config->{name} . "DB::Invoices")
    ->search({id => $c->request->captures->[1]})->first();
  $c->stash->{items} = [
    $c->model($c->config->{name} . "DB::InvoicesItems")
      ->search({invoice => $c->stash->{inv}->get_column("id")},
      {order_by => ["me.id ASC"]})->all()
  ];
  $c->stash->{currency} =
    $c->model($c->config->{name} . "DB::Currencies")
    ->search({id => $c->stash->{inv}->get_column("currency")})->first();
  $c->stash->{default_currency} =
    $c->model($c->config->{name} . "DB::Currencies")->search({default_c => 1})->first();

  $c->stash->{pdf_filename} = sprintf(
    "inv_%02s%08s_%s.pdf",
    $c->stash->{inv}->prefix,
    $c->stash->{inv}->number,
    $c->request->captures->[0]
  );
  my $font = $c->path_to('templates', 'LiberationSans-Regular.ttf');
  $c->stash->{font} = "$font";
  my $fontBold = $c->path_to('templates', 'LiberationSans-Bold.ttf');
  $c->stash->{fontBold} = "$fontBold";

  $c->stash->{pdf_template} = 'invoices/get_pdf.tt2';
  $c->forward($c->view('PDF::Reuse'));
  return;
}

sub prefixes : Local {
  my ($self, $c) = @_;

  if ($c->request->method() eq "POST") {
    $c->stash->{errors} = $c->form(required => [qw(prefix number)],);
    unless ($c->stash->{errors}->has_missing or $c->stash->{errors}->has_invalid) {
      my $query = {
        prefix => $c->request->params->{prefix},
        number => ($c->request->params->{number} - 1),
      };
      my $item = $c->model($c->config->{name} . "DB::Invoices")->search(
        { contractor => 0,
          prefix     => $c->request->params->{prefix}
        }
      )->first();
      unless ($item) {
        $c->model($c->config->{name} . "DB::Invoices")->create($query);
        $item = $c->model($c->config->{name} . "DB::Invoices")->search(
          { contractor => 0,
            prefix     => $c->request->params->{prefix}
          }
        )->first();
      }
      $item->update($query);
    }
  }

  my $maxes = [$c->model($c->config->{name} . "DB::Invoices")->search({},{
									  +select => [
										      \"MAX(id)",
										     ],
									  +as => [
										  qw/
										      id
										    /
										 ],
									  group_by => ['me.prefix']
									 })->all()];
  my $ids = [];
  for my $max (@$maxes) {
    push @$ids, $max->get_column('id');
  }
  $c->stash->{prefixes} = [
    $c->model($c->config->{name} . "DB::Invoices")->search(
      {
       id => $ids
      },
      { +select => [
		    "number",
		    "prefix"
		   ],
        +as => [
          qw/
            number
            prefix
            /
        ],
        group_by => ["me.prefix"],
        order_by => ["me.prefix ASC"]
      }
      )->all()
  ];
  return;
}

__PACKAGE__->meta->make_immutable;

1;
