package WfInvoice::Controller::Invoices::Export;
use Moose;
use namespace::autoclean;
use Date::Format;
use Date::Parse;
use Text::Iconv;
use Archive::Zip;
use IO::String;

=head1 LICENSE

    This file is part of WfInvoice -- the free invoicing software
    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

WfInvoice::Controller::Invoices::Export - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index : Path : Args(0) {
  my ($self, $c) = @_;
  if ($c->request->method() eq "POST") {
    $c->stash->{errors} = $c->form(required => [qw( from_date to_date product )],);
    unless ($c->stash->{errors}->has_missing or $c->stash->{errors}->has_invalid) {
      my $value = [
        $c->model($c->config->{name} . "DB::Invoices")->search(
          { issue_date => {
              "<=" => $c->request->params->{to_date},
              ">=" => $c->request->params->{from_date},
            }
          },
          { join    => ["items", "currency"],
            +select => [
              "me.id", "me.issue_date", "currency.sign",
              "SUM(items.price * items.quantity * (1 + me.vat/100) )",
              "me.prefix", "me.number", "me.currency", "me.rate", "me.type",
              "me.currency", "me.contractor",  "me.c_name", "me.c_city", "me.c_address",
              "me.c_eik",    "me.c_ddsnumber", "me.c_mol",  "me.vat",    "me.p_zdds",
              "me.novatreason",  "me.p_name",      "me.p_city", "me.p_address",
              "me.p_eik",        "me.p_ddsnumber", "me.p_mol",  "me.related_invoice",
              "me.related_date", "me.bank_payment"
            ],
            +as => [
              qw/
                id
                issue_date
                curr
                value
                prefix
                number
                currency
                rate
                type
                currency
                contractor
                c_name
                c_city
                c_address
                c_eik
                c_ddsnumber
                c_mol
                vat
                p_zdds
                novatreason
                p_name
                p_city
                p_address
                p_eik
                p_ddsnumber
                p_mol
                related_invoice
                related_date
                payment_method
                /
            ],
            order_by => ['me.id DESC'],
            group_by => ["me.id"],
          }
          )->all()
      ];
      $c->forward($c->action->class, $c->request->params->{product}, [$value]);
    }
  }
  return;
}

sub azhur : Private {
  my ($self, $c) = @_;
  my $invoices = $c->request->args->[0];
  my $rtot     = "";
  my $rdet     = "";
  for my $inv (@{$invoices}) {
    my $fields;
    my $detfields;
    $fields->[101] = "";
    $fields->[0] =
        sprintf("%02s", $inv->get_column("prefix"))
      . sprintf("%08s", $inv->get_column("number"));
    $fields->[1] = 0;
    $fields->[2] = time2str('%d.%m.%Y', str2time($inv->get_column("issue_date")));
    $fields->[3] = time2str('%d.%m.%Y', str2time($inv->get_column("event_date")));
    $fields->[4] = $inv->currency->get_column("currency");
    $fields->[5] = $inv->currency->get_column("rate");                            # BGN?
    my $type = 1;

    if ($inv->get_column("type") == 1) {
      $type = 4;
    }
    elsif ($inv->get_column("type") == 2) {
      $type = 3;
    }
    $fields->[6]  = $type;
    $fields->[7]  = $inv->get_column("currency") - 1;
    $fields->[8]  = $inv->get_column("contractor");
    $fields->[9]  = $inv->get_column("c_name");
    $fields->[10] = $inv->get_column("c_city") . ", " . $inv->get_column("c_address");
    $fields->[11] = $inv->get_column("c_ddsnumber");
    $fields->[12] = $inv->get_column("c_eik");
    my $vat_type = 5;

    if ($inv->get_column("p_zdds")) {
      if ($inv->get_column("vat") == 0) {
        $vat_type = 2;
      }
      elsif ($inv->get_column("vat") == 7) {
        $vat_type = 1;
      }
      elsif ($inv->get_column("vat") == 20) {
        $vat_type = 0;
      }
    }
    else {
      $vat_type = 4;
    }
    $fields->[13] = $vat_type;
    $fields->[14] = $inv->get_column("novatreason");
    $fields->[19] = $inv->get_column("p_name");
    $fields->[20] = $inv->get_column("p_city") . ", " . $inv->get_column("p_address");
    $fields->[21] = $inv->get_column("p_ddsnumber");
    $fields->[22] = $inv->get_column("p_eik");

    $fields->[24] = (($type > 1) ? ($inv->get_column("related_invoice")) : "");
    $fields->[25] = (
        ($type > 1)
      ? (time2str('%d.%m.%Y', str2time($inv->get_column("related_date"))))
      : ""
    );
    $fields->[26] = (($type > 1) ? "Няма" : "");
    my @methods = (0, 1, 2, 3, 2, 4, 2, 2);
    $fields->[27] =
      $methods[$inv->get_column("payment_method")];    # Начин на плащане
    $fields->[30] = time2str('%d.%m.%Y', str2time($inv->get_column("issue_date")));
    $fields->[31] = 0;

    $fields->[37] = sprintf "%.2f",
      $inv->get_column("value")
      * (($inv->get_column("vat") > 0) ? (1 - $inv->get_column("vat") / 100) : 0);
    $fields->[36] = $fields->[37];
    $fields->[38] = sprintf "%.2f",
      $inv->get_column("value")
      * (($inv->get_column("vat") > 0) ? ($inv->get_column("vat") / 100) : 0);
    $fields->[39] = sprintf("%.2f", $inv->get_column("value"));
    $fields->[40] = 0;
    $fields->[41] = 0;

    $fields->[43] = sprintf "%.2f",
        $inv->get_column("value") 
      * $inv->get_column("rate")
      * (($inv->get_column("vat") > 0) ? (1 - $inv->get_column("vat") / 100) : 0);
    $fields->[42] = $fields->[43];
    $fields->[44] = sprintf "%.2f",
        $inv->get_column("value") 
      * $inv->get_column("rate")
      * (($inv->get_column("vat") > 0) ? ($inv->get_column("vat") / 100) : 0);
    $fields->[45] = sprintf "%.2f",
      $inv->get_column("value") * $inv->get_column("rate");
    $fields->[46] = 0;
    $fields->[47] = 0;
    $fields->[48] = 1;
    $fields->[49] = $inv->get_column("p_zdds");    #Ако съм рег. по ЗДДС

    $fields->[66] = "Стока/Услуга"
      ;    # Вид на стоката - свободен текст
    $fields->[67] = 0;    # Не е Аванс

    my $lcount = 0;
    for my $item ($inv->items->all()) {
      $detfields->[33] = "";
      $detfields->[0]  = $fields->[0];
      $detfields->[1]  = $fields->[1];

      $detfields->[2]  = ++$lcount;
      $detfields->[10] = $item->mesurement;
      $detfields->[11] = sprintf "%.3f", $item->quantity;
      $detfields->[12] = 0;

      $detfields->[18] = sprintf "%.2f", $item->price;
      $detfields->[20] = sprintf "%.2f", $item->price * $inv->get_column("rate");

      $rdet .= join("|", @{$detfields});
      $rdet .= ";\n";
    }

    $rtot .= join("|", @{$fields});
    $rtot .= ";\n";
  }

  my $converter = Text::Iconv->new("utf8", "cp1251");
  my $zip = Archive::Zip->new();
  $zip->addString($converter->convert($rtot),
    'rtot' . $c->request->params->{to_date} . '.eid');
  $zip->addString($converter->convert($rdet),
    'rdet' . $c->request->params->{to_date} . '.eid');
  $c->response->content_type("application/zip");
  $c->response->headers->header("Content-Disposition" => '"inline"; filename="export_'
      . $c->request->params->{to_date}
      . '.zip"');
  my $mf = "";
  my $fh = IO::String->new(\$mf);
  $zip->writeToFileHandle($fh);
  $fh->close();
  $c->response->body($mf);
  return;
}

sub delta : Private {
  my ($self, $c) = @_;
  my $invoices  = $c->request->args->[0];
  my $rtot      = "";
  my $converter = Text::Iconv->new("utf8", "cp1251");
  for my $inv (@{$invoices}) {
    my $fields;
    $fields->[0] = "2";    # Тип на операцията
    $fields->[1] =
      time2str('%d.%m.%Y', str2time($inv->get_column("issue_date")))
      ;                    # Дата на издаване
    $fields->[2] =
        sprintf("%02s", $inv->get_column("prefix"))
      . sprintf("%08s", $inv->get_column("number"))
      ;                    # номер на документа
    my $type = "Ф-ра";
    if ($inv->get_column("type") == 1) {
      $type = "КИ";
    }
    elsif ($inv->get_column("type") == 2) {
      $type = "ДИ";
    }
    $fields->[3] = $type;    # Тип на документа
    $fields->[4] =
      sprintf("%.2f", $inv->get_column("value") * $inv->get_column("rate"))
      ;                      # Стойност

    my $vat_type = 9;        # Стонйност по подразбиране
    if ($inv->get_column("p_zdds")) {
      if ($inv->get_column("vat") == 0) {    # 0%
        $vat_type = 21;
      }
      elsif ($inv->get_column("vat") == 7) {    # 7%
        $vat_type = 20;
      }
      elsif ($inv->get_column("vat") == 20) {    # 20%
        $vat_type = 16;
      }
    }
    else {
      $vat_type = 9;                             # Нерег. по ЗДДС
    }
    $fields->[5]  = $vat_type;
    $fields->[6]  = $inv->get_column("c_name");
    $fields->[7]  = $inv->get_column("c_mol");
    $fields->[8]  = $inv->get_column("c_city");
    $fields->[9]  = $inv->get_column("c_address");
    $fields->[10] = $inv->get_column("c_ddsnumber");
    $fields->[11] = $inv->get_column("c_eik");

    $fields->[15] = sprintf "%.2f",
        $inv->get_column("value") 
      * $inv->get_column("rate")
      * (($inv->get_column("vat") > 0) ? ($inv->get_column("vat") / 100) : 0);
    $rtot .= join("|", @{$fields});
    $rtot .= "\r\n";
  }
  $c->response->content_type('text/plain; charset="windows-1251"');
  $c->response->headers->header(
    "Content-Disposition" => qq{"inline"; filename="Import.txt"});
  $c->response->body($converter->convert($rtot));
  return;
}

__PACKAGE__->meta->make_immutable;

1;
