package WfInvoice::Controller::Config;
use Moose;
use namespace::autoclean;
use Config::General;
use File::Copy;
use utf8;
BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

WfInvoice::Controller::Config - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index : Path : Args(0) {
  my ($self, $c) = @_;
  my $data;
  my $conf   = Config::General->new($c->path_to('wfinvoice_local.conf'));
  my $config = {$conf->getall};
  if ($c->request->method eq "POST") {
    $c->stash->{errors} = $c->form(
      required => [
        qw(
          p_name
          p_city
          p_eik
          p_address
          p_mol
          vat
          digits_price
          digits_sprice
          digits_qty
          default_payment_method
          mesures
          )
      ],
    );

    unless ($c->stash->{errors}->has_missing or $c->stash->{errors}->has_invalid) {
      copy($c->path_to('wfinvoice_local.conf'), $c->path_to('wfinvoice_local.conf~'));

      $config->{company}->{name}                 = $c->request->params->{p_name};
      $config->{company}->{eik}                  = $c->request->params->{p_eik};
      $config->{company}->{dds}                  = $c->request->params->{p_ddsnumber};
      $config->{company}->{name}                 = $c->request->params->{p_name};
      $config->{company}->{mol}                  = $c->request->params->{p_mol};
      $config->{company}->{city}                 = $c->request->params->{p_city};
      $config->{company}->{address}              = $c->request->params->{p_address};
      $config->{company}->{bank_account}->{iban} = $c->request->params->{p_iban};
      $config->{company}->{bank_account}->{bic}  = $c->request->params->{p_bic};
      $config->{company}->{bank_account}->{bank} = $c->request->params->{p_bank};

      $config->{vat} = ($c->request->params->{vat} + 100) / 100;

      $config->{vat_precalc} = $c->request->params->{vat_precalc};

      $config->{digits}->{price}  = $c->request->params->{digits_price};
      $config->{digits}->{sprice} = $c->request->params->{digits_sprice};
      $config->{digits}->{qty}    = $c->request->params->{digits_qty};

      my $payment_methods = $c->request->params->{payment_methods};
      $payment_methods =~ s/\s*,\s*/,/gx;
      $payment_methods =~ s/\n//gx;
      $config->{payment_methods} = [split ",", $payment_methods];

      my $mesures = $c->request->params->{mesures};
      $mesures =~ s/\s*,\s*/,/gx;
      $mesures =~ s/\n//gx;
      $config->{mesures} = [split ",", $mesures];

      $config->{default_payment_method} = $c->request->params->{default_payment_method};

      $conf->save_file($c->path_to('wfinvoice_local.conf.tmp'), $config);
      move($c->path_to('wfinvoice_local.conf.tmp'),
        $c->path_to('wfinvoice_local.conf'));

      $c->config->{company}                = $config->{company};
      $c->config->{vat}                    = $config->{vat};
      $c->config->{vat_precalc}            = $config->{vat_precalc};
      $c->config->{digits}                 = $config->{digits};
      $c->config->{payment_methods}        = $config->{payment_methods};
      $c->config->{default_payment_method} = $config->{default_payment_method};
      $c->config->{mesures}                = $config->{mesures};
    }
    $c->response->redirect($c->uri_for("/config"));
  }

  $c->stash->{payment_methods} =
    [$c->model($c->config->{name} . "DB::PaymentMethods")
      ->search({}, {order_by => "me.id ASC"})->all()
    ];

  $data->{vat}                    = ($c->config->{vat} - 1) * 100;
  $data->{vat_precalc}            = $c->config->{vat_precalc};
  $data->{default_payment_method} = $c->config->{default_payment_method};
  $data->{mesures}                = join ", ", @{$c->config->{mesures}};
  $c->forward($c->view('TT'));
  $c->fillform($data);
  return;
}


=head1 AUTHOR

Anton Katsarov,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
