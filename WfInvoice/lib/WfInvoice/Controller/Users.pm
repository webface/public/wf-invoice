package WfInvoice::Controller::Users;
use utf8;

=head1 LICENSE

    This file is part of WfInvoice -- the free invoicing software
    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use Moose;
use namespace::autoclean;
use Digest::SHA qw(sha1);

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

WfInvoice::Controller::Users - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub page : LocalRegex('^page\-(\d+)$') {
  my ($self, $c) = @_;
  $c->forward('WfInvoice::Controller::User', 'index', [$c->request->snippets->[0]]);
  return;
}

sub index : Path : Args(0) {
  my ($self, $c) = @_;

  my $page = 1;
  if ($c->request->args->[0]) {
    $page = $c->request->args->[0];
    $c->response->redirect('/user') if $page == 1;
  }
  my $query = {};
  if ($c->request->params->{usersearch}) {
    $query =
      {'CONCAT(username, " ", realname, " ", email)' =>
        {"LIKE", "%" . $c->request->params->{usersearch} . "%"}};
  }
  $c->stash->{users} = [
    $c->model($c->config->{name} . 'DB::Users')->search(
      $query,
      { rows     => $c->config->{rows_per_page_users},
        page     => $page,
        order_by => ['username']
      }
      )->all()
  ];
  my $count = $c->model($c->config->{name} . "DB::Users")->search(
    {},
    { select => [
		 \"COUNT(me.id)"
      ],
      as => [
        qw/
          count
          /
      ]
    }
  )->first()->get_column('count');
  $c->forward('WfInvoice::Controller::Root', 'pager',
    [$count, $page, $c->config->{rows_per_page}, '/users/page']);

  $c->stash->{template} = 'users/index.tt2';
  return;
}

sub edit : LocalRegex("^edit/(\d+)$") {
  my ($self, $c) = @_;
  my $data;

  $c->stash->{roles} = [$c->model($c->config->{name} . 'DB::Roles')->search({}, {})];
  if ($c->request->method() eq "POST") {
    my $query = {
      realname      => $c->request->params->{'realname'},
      'sign_prefix' => $c->request->params->{'sign_prefix'},
    };
    if ($c->request->params->{pass}) {
      $query->{password} = unpack("H*", sha1($c->request->param('pass')));
    }
    $c->model($c->config->{name} . "DB::Users")
      ->search({id => $c->request->captures->[0]})->update($query);

    $c->model($c->config->{name} . "DB::UserRole")
      ->search({user_id => $c->request->captures->[0]})->delete();
    for (@{$c->stash->{roles}}) {
      if ($c->request->params->{'role-' . $_->id}) {
        $c->model($c->config->{name} . "DB::UserRole")->create(
          { user_id => $c->request->captures->[0],
            role_id => $_->id
          }
        );
      }
    }
  }

  my $item =
    $c->model($c->config->{name} . "DB::Users")
    ->search({id => $c->request->snippets->[0]})->first();
  for (@{[$c->model($c->config->{name} . "DB::Users")->result_source->columns]}) {
    $data->{$_} = $item->get_column($_);
  }

  my $roles =
    [$c->model($c->config->{name} . "DB::UserRole")
      ->search({user_id => $c->request->snippets->[0]})->all()
    ];
  for (@{$roles}) {
    $data->{'role-' . $_->role_id} = 1;
  }


  $c->stash->{template} = "users/edit.tt2";
  $c->forward($c->view('TT'));
  $c->fillform($data);
  return;
}

sub add : Local {
  my ($self, $c) = @_;
  my $data;

  $c->stash->{roles} = [$c->model($c->config->{name} . 'DB::Roles')->search({}, {})];
  if ($c->request->method() eq "POST") {
    my $query = {
      username    => $c->request->params->{'username'},
      realname    => $c->request->params->{'realname'},
      sign_prefix => $c->request->params->{'sign_prefix'},
    };
    if ($c->request->params->{pass}) {
      $query->{password} = unpack("H*", sha1($c->request->param('pass')));
    }
    my $user = $c->model($c->config->{name} . "DB::Users")->create($query);

    for (@{$c->stash->{roles}}) {
      if ($c->request->params->{'role-' . $_->id}) {
        $c->model($c->config->{name} . "DB::UserRole")->create(
          { user_id => $user->id,
            role_id => $_->id
          }
        );
      }
    }
    $c->response->redirect("/users");
  }
  return;
}


sub status : Local {
  my ($self, $c) = @_;
  $c->stash->{template} = 'users/status.tt2';
  if ($c->request->param('usr')) {
    $c->model($c->config->{name} . 'DB::Users')
      ->search({username => $c->request->param('usr')},)
      ->update({status => $c->request->param('stat')});
  }
  return;
}

sub roles : Local {
  my ($self, $c) = @_;

  $c->stash->{roles} = [$c->model($c->config->{name} . 'DB::Roles')->search({}, {})];
  $c->stash->{users} = [
    $c->model($c->config->{name} . 'DB::Users')->search(
      {},
      { select => ['username', 'id', 'realname'],
        where => [status => 1]
      }
    )
  ];


  if ($c->request->param('submit')) {
    for my $role (@{$c->stash->{roles}}) {
      for my $user (@{$c->stash->{users}}) {
        if ($c->request->param($user->id . '/' . $role->id)) {
          $c->model($c->config->{name} . 'DB::UserRole')->find_or_create(
            { user_id => $user->id,
              role_id => $role->id
            }
          );
        }
        else {
          $c->model($c->config->{name} . 'DB::UserRole')->search(
            { user_id => $user->id,
              role_id => $role->id
            },
            {rows => 1}
          )->delete();
        }
      }
    }
  }


  $c->stash->{userroles} = [
    $c->model($c->config->{name} . 'DB::Users')->search(
      {},
      { select => ['username', 'id', 'realname'],
        join   => ['map_user_role'],
        where => [status => 1]
      }
    )
  ];

  my $role_set =
    [$c->model($c->config->{name} . 'DB::UserRole')->search({}, {})->all()];
  my $values = {};
  for (@{$role_set}) {
    $values->{$_->user_id . '/' . $_->role_id} = 1;
  }


  $c->stash->{template} = 'users/role.tt2';
  $c->forward($c->view('TT'));
  $c->fillform($values);
  return;
}


__PACKAGE__->meta->make_immutable;

1;
