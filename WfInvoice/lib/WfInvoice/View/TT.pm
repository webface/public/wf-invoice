package WfInvoice::View::TT;
use utf8;

=head1 NAME

WfInvoice::View::TT - Catalyst TTSite View

=head1 SYNOPSIS

See L<WfInvoice>

=head1 DESCRIPTION

Catalyst TTSite View.

=head1 LICENSE

    This file is part of WfInvoice -- the free invoicing software
    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut


use strict;
use warnings;
use base 'Catalyst::View::TT';

__PACKAGE__->config(
  { INCLUDE_PATH =>
      [WfInvoice->path_to('root', 'src'), WfInvoice->path_to('root', 'lib')],
    TEMPLATE_EXTENSION => '.tt2',
    PRE_PROCESS        => 'config/main',
    WRAPPER            => 'site/wrapper',
    ERROR              => 'error.tt2',
    TIMER              => 0,
    render_die         => 1,
  }
);

1;

