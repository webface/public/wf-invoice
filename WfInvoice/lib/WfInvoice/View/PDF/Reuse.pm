package WfInvoice::View::PDF::Reuse;

=head1 NAME

WfInvoice::View::PDF::Reuse - PDF::Reuse View for WfInvoice

=head1 DESCRIPTION

PDF::Reuse View for WfInvoice.

=head1 AUTHOR

Anton Katsarov,,,

=head1 SEE ALSO

L<WfInvoice>

=head1 NAME

WfInvoice::View::Ajax - TT View for WfInvoice

=head1 DESCRIPTION

TT View for WfInvoice.

=head1 SEE ALSO

L<WfInvoice>

=head1 AUTHOR

Антон Кацаров,,,,

=head1 LICENSE

    This file is part of WfInvoice -- the free invoicing software
    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use strict;
use warnings;
use base 'Catalyst::View::PDF::Reuse';

1;
