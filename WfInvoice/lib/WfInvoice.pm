package WfInvoice;

=head1 NAME

WfInvoice - Free Invoicing

=head1 SYNOPSIS

    script/wfinvoice_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<WfInvoice::Controller::Root>, L<Catalyst>

=head1 LICENSE

    This file is part of WfInvoice -- the free invoicing software
    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use Moose;
use namespace::autoclean;

use Catalyst::Runtime 5.80;
use utf8;

# Set flags and add plugins for the application
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

use Catalyst qw/
  ConfigLoader
  Static::Simple
  FormValidator
  Authentication
  Authorization::Roles
  Authorization::ACL
  Session
  Session::Store::File
  Session::State::Cookie
  FillInForm
  /;

extends 'Catalyst';

our $VERSION = '0.0.3';
$VERSION = eval $VERSION;

# Configure the application.
#
# Note that settings in wfinvoice.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

__PACKAGE__->config(
  name         => 'WfInvoice',
  default_view => 'TT',          # setting prefered view

  # Disable deprecated behavior needed by old applications
  disable_component_resolution_regex_fallback => 1,
  'View::PDF::Reuse'                          => {
    INCLUDE_PATH => __PACKAGE__->path_to('root', 'src'),
    ENCODING     => 'utf8'
  },
);

# Start the application
__PACKAGE__->setup();

__PACKAGE__->deny_access_unless("/users",             [qw/user_management/]);
__PACKAGE__->deny_access_unless("/invoices/prefixes", [qw/invoices_settings/]);
__PACKAGE__->deny_access_unless("/config",            [qw/global_settings/]);

1;
