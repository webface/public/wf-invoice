package Template::Plugin::Price2WordsBG;
use strict;
use warnings;

=head1 LICENSE

    Template::Plugin::Price2WordsBG -- converts price to words in
    Bulgarian

    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut


use Template::Plugin::Filter;
use base qw( Template::Plugin::Filter );

our $DYNAMIC = 1;

our %l1 = (
  1  => "един",
  2  => "два",
  3  => "три",
  4  => "четири",
  5  => "пет",
  6  => "шест",
  7  => "седем",
  8  => "осем",
  9  => "девет",
  10 => "десет",
  11 => "единадесет",
  12 => "дванадесет",
  13 => "тринадесет",
  14 => "четиринадесет",
  15 => "петнадесет",
  16 => "шестнадесет",
  17 => "седемнадесет",
  18 => "осемнадесет",
  19 => "деветнадесет",
);
my %l2 = (
  20 => "двадесет",
  30 => "тридесет",
  40 => "четиридесет",
  50 => "петдесет",
  60 => "шестдесет",
  70 => "седемдесет",
  80 => "осемдесет",
  90 => "деветдесет",
);

my %l3 = (
  100 => "сто",
  200 => "двеста",
  300 => "триста",
  400 => "четиристотин",
  500 => "петстотин",
  600 => "шестстотин",
  700 => "седемстотин",
  800 => "осемстотин",
  900 => "деветстотин",
);

sub filter {
  my ($self, $price) = @_;
  my $conf = $self->{_CONFIG};
  if ($conf->{g}) {
    my @str = split ",", $conf->{g};
    $l1{1} = "ед" . $str[0];
    $l1{2} = "дв" . $str[1];
  }
  return digtochar_bg($price, $conf->{curr}, $conf->{coins});
}

sub digtochar_bg {
  my $in    = shift;
  my $prfx  = shift || 'лв.';
  my $sprfx = shift || 'ст.';
  $in = 0 if !$in;

  # if ($in !~ /^\d+$|^\d+\D\d+$|^\D\d+$|^\d+\D$/) {
  #   print "Неправилно въведени данни! \n";exit;
  # }
  # ;
  my ($c, $d);
  if ($in =~ /\D/x) {
    ($c, $d) = split /\D/x, $in;
    $c = 0 if !$c;
  }
  else {
    $c = $in;
    $d = 0;
  }
  my $gogo = num2ch($c);
  if ($c == 0) {
    $gogo = "нула";
  }
  $gogo =~ s/ин\ хиляд/на\ хиляд/x;
  $gogo =~ s/ва\ хиляд/ве\ хиляд/x;
  $d = sprintf("%02i", substr($d . "000", 0, 2));
  return $gogo . " $prfx и " . $d . " " . $sprfx;
}

sub num2ch {
  my $in = shift;

  if ($in < 20) {
    return $l1{$in};
  }
  elsif ($in < 100) {
    if ($in % 10) {
      return $l2{int($in / 10) * 10} . " и " . $l1{$in % 10};
    }
    else {
      return $l2{int($in / 10) * 10};
    }
  }
  elsif ($in < 1000) {
    if ((($in % 100 < 20) || ($in % 10 == 0)) && num2ch($in % 100)) {
      return $l3{int($in / 100) * 100} . " и " . num2ch($in % 100);
    }
    else {
      return $l3{int($in / 100) * 100} . " " . num2ch($in % 100);
    }
  }
  elsif ($in < 1000000) {
    if ( (int($in / 100) % 10 == 0)
      && (($in % 100 < 20) || ($in % 10 == 0))
      && num2ch($in % 1000))
    {
      return l4(int($in / 1000) * 1000) . " и " . num2ch($in % 1000);
    }
    else {
      return l4(int($in / 1000) * 1000) . " " . num2ch($in % 1000);
    }
  }
  elsif ($in < 1000000000) {
    if ( (int($in / 100) % 10000 == 0)
      && (($in % 100 < 20) || ($in % 10 == 0))
      && num2ch($in % 1000000))
    {
      return l5(int($in / 1000000) * 1000) . " и " . num2ch($in % 1000000);
    }
    else {
      return l5(int($in / 1000000) * 1000) . " " . num2ch($in % 1000000);
    }
  }
}

sub l4 {
  my $in = shift;

  if ($in == 1000) {
    return "хиляда";
  }
  else {
    return num2ch(int($in / 1000)) . " хиляди";
  }
}

sub l5 {
  my $in = shift;
  if ($in == 1000) {
    return num2ch(int($in / 1000)) . " милион";
  }
  else {
    return num2ch(int($in / 1000)) . " милионa";
  }
}

1;
