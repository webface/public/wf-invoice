package WfInvoiceDB::Invoices;
use strict;
use warnings;

=head1 LICENSE

    This file is part of WfInvoice -- the free invoicing software
    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use base qw/DBIx::Class/;

# Load required DBIC stuff
__PACKAGE__->load_components(qw/PK::Auto Core/);

# Set the table name
__PACKAGE__->table('invoices');

# Set columns in table
__PACKAGE__->add_columns(
  qw/id
    prefix
    number
    contractor
    issue_date
    event_date
    receiver
    bank_payment
    c_name
    c_city
    c_address
    c_eik
    c_ddsnumber
    c_mol
    c_person
    c_egn
    currency
    rate
    vat
    novatreason
    type
    related_invoice
    related_date
    p_name
    p_city
    p_address
    p_eik
    p_ddsnumber
    p_mol
    p_bank
    p_iban
    p_bic
    p_zdds
    author
    author_user
    author_sign
    /
);

# Set the primary key for the table
__PACKAGE__->set_primary_key(qw/id/);

__PACKAGE__->has_many(items => 'WfInvoiceDB::InvoicesItems', 'invoice');
__PACKAGE__->has_one(
  contractor => 'WfInvoiceDB::Contractors',
  {'foreign.id' => 'self.contractor'}
);
__PACKAGE__->has_one(
  currency => 'WfInvoiceDB::Currencies',
  {'foreign.id' => 'self.currency'}
);
__PACKAGE__->has_one(
  payment_method => 'WfInvoiceDB::PaymentMethods',
  {'foreign.id' => 'self.bank_payment'}
);
1;
