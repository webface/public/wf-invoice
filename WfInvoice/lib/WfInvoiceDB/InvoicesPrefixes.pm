package WfInvoiceDB::InvoicesPrefixes;
use utf8;
use strict;
use warnings;

=head1 LICENSE

    This file is part of WfInvoice -- the free invoicing software
    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use base qw/DBIx::Class::Core/;

# Load required DBIC stuff
# Set the table name
__PACKAGE__->table('invoices');

# Set columns in table
__PACKAGE__->add_columns(
  qw/
    prefix
    author_user
    /
);
__PACKAGE__->result_source_instance->name(
  \'(SELECT * FROM (SELECT prefix AS prefix, author_user AS author_user, CASE author_user WHEN ? THEN 1 ELSE 0 END AS sortID from invoices order by sortID DESC) AS t GROUP BY prefix ORDER by sortID DESC, prefix ASC)'
);

1;
