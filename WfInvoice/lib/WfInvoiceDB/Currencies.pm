package WfInvoiceDB::Currencies;
use strict;
use warnings;

=head1 LICENSE

    This file is part of WfInvoice -- the free invoicing software
    Copyright (C) 2012 Anton Katsarov <anton@webface.bg>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use base qw/DBIx::Class/;

# Load required DBIC stuff
__PACKAGE__->load_components(qw/PK::Auto Core/);

# Set the table name
__PACKAGE__->table('currencies');

# Set columns in table
__PACKAGE__->add_columns(
  qw/id currency rate sign subsign longsign default_c updated g/);

# Set the primary key for the table
__PACKAGE__->set_primary_key(qw/id/);

1;
