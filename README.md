# WebFaceInvoice

WfInvoice is free Bulgarian invoicing software. The content below is in Bulgarian because this software is mostly useful for Bulgarian users.

## Относно

WfInvoice е уеб базиран свободен софтуер за фактуриране, който може да работи под GNU/Linux операционна система. Благодарение на факта, че е базиран на Catalyst Web Framework, той лесно може да бъде инсталиран на всеки уеб сървър, хостинг или да бъде използван самостоятелно.

## Лиценз

Тази програма е свободен софтуер и се разпространява под [GNU/AGPL v3](http://www.gnu.org/licenses/agpl.txt) лиценз.

## Изтегляне

Чрез Git:

```
  git clone https://git.webface.bg/public-projects/wf-invoice.git
```

## Инструкции за инсталиране

Примерното исталиране по-долу е описано за Debian базирани системи.

Инсталирайте необходимите зависимости:

```
  user@debian:~$ sudo aptitude install libcatalyst-perl libcatalyst-modules-perl dh-make-perl libhtml-fillinform-perl libdata-formvalidator-perl libpdf-reuse-perl
```

Тъй като не всички необходими пакети ги има в стандартната дистрибуция, е необходимо да се пакетират. За това ще използваме инструмента `dh-make-perl`, който вече инсталирахме.

```
  user@debian:~$ cd /tmp

  user@debian:/tmp$ dh-make-perl --build --cpan Catalyst::Plugin::FillInForm
  user@debian:/tmp$ sudo dpkg -i libcatalyst-plugin-fillinform-perl\_0.06-1\_all.deb

  user@debian:/tmp$ dh-make-perl --build --cpan Catalyst::Plugin::FormValidator
  user@debian:/tmp$ sudo dpkg -i libcatalyst-plugin-formvalidator-perl\_0.094-1\_all.deb

  user@debian:/tmp$ dh-make-perl --build --cpan Catalyst::View::PDF::Reuse
  user@debian:/tmp$ sudo dpkg -i libcatalyst-view-pdf-reuse-perl\_0.04-1\_all.deb

  user@debian:/tmp$ dh-make-perl --build --cpan Template::Plugin::Decode
  user@debian:/tmp$ sudo dpkg -i libtemplate-plugin-decode-perl\_0.02-1\_all.deb
```

Сега всички необходими зависимости са инсталирани. Необходимо е да инсталирате базата данни. Ако вече нямате инсталиран `mysql-server` на системата си можете да го направите по следния начин:

```
  user@debian:/tmp$ sudo aptitude install mysql-server
```

Ако ползвате Debian базирана дистрибуция по време на инсталацията трябва да бъдете запитани за парола на основния потребител _root_. След инсталацията създайте необходимата база данни, като можете да заместите името на базата данни с желаната от Вас:

```
  user@debian:/tmp$ mysql -u root -p
  Enter password:

  mysql> create database invoice;
  Query OK, 1 row affected (0.00 sec)

  mysql> use invoice;
  Database changed
```

След това инсталирайте необходимите таблици и данни в нея:

```
  mysql> \\. /home/user/WfInvoice-ebf5381/sql/db.sql
```

Преди да изпълните преходната команда проверете местонахождението на файла `db.sql` на вашата система. След изпълнението на предходната команда може да изтриете файла `db.sql`, той вече не ни е необходим.

След това трябва да разрешим на потребител достъп до базата данни с необходимите правомощия като заместите потребителското име и парола с желаните от Вас:

  mysql> grant all on invoice.* to invoice\_user@localhost identified by "invoice\_pass";
  Query OK, 0 rows affected (0.00 sec)

```
  mysql> exit
  Bye
```

Следва да свържем нашето приложение с базата данни. За целта трябва да се върнем в разархивираната вече директория и да направим необходимите промени в конфигурационния файл:

```
  user@debian:/tmp$ cd ~/wf-invoice/
  user@debian:~/wf-invoice$ gedit wfinvoice.conf
```

След като отворите файла с любимия си редактор променете следните редове като попълните правилните име на база данни, потребите и парола:

```
  dbi_dsn  dbi:mysql:invoice
  dbi\_user invoice\_user
  dbi\_pass invoice\_pass
```

Направете изпълними необходимите скриптове:

  user@debian:~/wf-invoice$ chmod +x script/*

След това сте готови да стартирате Вашия софтуер за фактуриране:

```
  user@debian:~/wf-invoice$ ./script/wfinvoice_server.pl
  You can connect to your server at http://debian:3000

```

Както вече може би забелязвате можете да да отворите в любимия Ви браузър адреса, който е изписан в терминала. За да спрете изпълнението на сървъра натиснете Ctrl+C.

Потребителят и паролата по подразбиране са admin. Ако желаете след вход в системата можете да смените своята парола или да добавите още потребители.

Ако не обичате да използвате терминал и да не Ви се налага да стартирате приложението всеки път по този начин можете да създадете стартер на вашия Работен Плот или панел. В следващото видео е посочено как става това в GNOME:



Или можете да изтеглите видеото: [wfinvoiceIcon.ogg \[ogg/Theora | 5MB\]](http://code.webface.bg/videos/wfinvoiceIcon.ogg)

## Контакт с нас

Ако имате затруднения с използването или инсталирането на приложението или просто имате въпроси не се колебайте да се [свържете с нас](http://www.webface.bg/contacts).
